# Joins

1. Write a query to find the addresses (location_id, street_address, city, state_province, country_name) of all the departments. Go to the editor
Hint : Use NATURAL JOIN.
Sample table : locations
Sample table :countries

* Answer :

2. Write a query to find the names (first_name, last name), department ID and the name of all the employees. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

3. Write a query to find the employee id, name (last_name) along with their manager_id, manager name (last_name). Go to the editor
Sample table : employees

* Answer :

4. Write a query to find the names (first_name, last_name) and hire date of the employees who were hired after 'Jones'. Go to the editor
Sample table : employees

* Answer :

5. Write a query to get the department name and number of employees in the department. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

6. Write a query to find the employee ID, job title number of days between ending date and starting date for all jobs in department 90 from job history. Go to the editor
Sample table : employees

* Answer :

7. Write a query to display the department ID, department name, and manager first name. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

8. Write a query to display the department name, manager name, and city. Go to the editor
Sample table : employees
Sample table : departments
Sample table : locations

* Answer :

9. Write a query to display the job title and average salary of employees. Go to the editor
Sample table : employees

* Answer :

10. Write a query to to display job title, employee name, and the difference between the salary of the employee and minimum salary for the job. Go to the editor
Sample table : employees

* Answer :

11. Write a query to display the job history that was done by any employee who is currently drawing more than 10000 of salary. Go to the editor
Sample table : employees
Sample table : Job_history

* Answer :
