# Les exercices présenté ici et leur solution se trouvent sur https://www.w3resource.com/sqlite-exercises/ 
# NE PAS REGARDER LA SOLUTION AVANT DE FAIRE L'EXERCICE

* Le but est de répondre aux questions, de faire au minimum 1 commit par partie de de le mettre en ligne
* NE PAS faire de commit directement sur master

### 1: Retrieve data from tables 
(https://www.w3resource.com/sql-exercises/sql-retrieve-from-table.php#SQLEDITOR)
* File : RetrieveDataFromTables.md
### 2: Restricting and Sorting Data
(https://www.w3resource.com/sqlite-exercises/sqlite-restricting-and-sorting-data-exercises.php)
* File : RestrictingAndSortingData.md
### 3: Aggregate Functions
(https://www.w3resource.com/sqlite-exercises/sqlite-aggregate-functions-group-by-exercises.php)
* File : AggregateFunctions.md
### 4: Subquery 
(https://www.w3resource.com/sqlite-exercises/sqlite-subquery-exercises.php)
* File : Subquery.md
### 5: Joins 
(https://www.w3resource.com/sqlite-exercises/sqlite-join-exercises.php)
* File : Joins.md
