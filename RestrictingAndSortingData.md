# Restricting and Sorting Data

1. Write a query to display the names (first_name, last_name) and salary for all employees whose salary is not in the range $10,000 through $15,000. Go to the editor
Sample table: employees

* Answer :

2. Write a query to display the names (first_name, last_name) and department ID of all employees in departments 30 or 100 in ascending alphabetical order by department ID. Go to the editor
Sample table : employees

* Answer :

3. Write a query to display the names (first_name, last_name) and salary for all employees whose salary is not in the range $10,000 through $15,000 and are in department 30 or 100. Go to the editor
Sample table : employees

* Answer :

4. Write a query to display the first_name of all employees who have both an "b" and "c" in their first name. Go to the editor
Sample table : employees

* Answer :

5. Write a query to display the last name, job, and salary for all employees whose job is that of a Programmer or a Shipping Clerk, and whose salary is not equal to $4,500, $10,000, or $15,000. Go to the editor
Sample table : employees

* Answer :

6. Write a query to display the last names of employees whose names have exactly 6 characters. Go to the editor
Sample table : employees

* Answer :

7. Write a query to display the last names of employees having 'e' as the third character. Go to the editor
Sample table : employees

* Answer :

8. Write a query to display the jobs/designations available in the employees table. Go to the editor
Sample table : employees

* Answer :

9. Write a query to display the names (first_name, last_name), salary and PF (15% of salary) of all employees. Go to the editor
Sample table : employees

* Answer :
