# Retrieve data from tables 


1. Write a SQL statement to display all the information of all salesmen.   
Sample table: salesman

* Answer : 

2. Write a SQL statement to display a string "This is SQL Exercise, Practice and Solution".   

* Answer : 

3. Write a query to display three numbers in three columns.   

* Answer : 

4. Write a query to display the sum of two numbers 10 and 15 from RDMS sever.   

* Answer : 

5. Write a query to display the result of an arithmetic expression.   

* Answer : 

6. Write a SQL statement to display specific columns like name and commission for all the salesmen.    
Sample table: salesman

* Answer : 

7. Write a query to display the columns in a specific order like order date, salesman id, order number and purchase amount from for all the orders.    
Sample table: orders

* Answer : 

8. Write a query which will retrieve the value of salesman id of all salesmen, getting orders from the customers in orders table without any repeats.   
Sample table: orders

* Answer : 

9. Write a SQL statement to display names and city of salesman, who belongs to the city of Paris.   
Sample table: salesman

* Answer : 

10. Write a SQL statement to display all the information for those customers with a grade of 200.   
Sample table: customer

* Answer : 

11. Write a SQL query to display the order number followed by order date and the purchase amount for each order which will be delivered by the salesman who is holding the ID 5001.   
Sample table: orders

* Answer : 

12. Write a SQL query to display the Nobel prizes for 1970.   
Sample table: nobel_win

* Answer : 

13. Write a SQL query to know the winner of the 1971 prize for Literature.   
Sample table: nobel_win

* Answer : 

14. Write a SQL query to display the year and subject that won 'Dennis Gabor' his prize.   
Sample table: nobel_win

* Answer : 

15. Write a SQL query to give the name of the 'Physics' winners since the year 1950.   
Sample table: nobel_win

* Answer : 

16. Write a SQL query to Show all the details (year, subject, winner, country ) of the Chemistry prize winners between the year 1965 to 1975 inclusive.   
Sample table: nobel_win

* Answer : 

17. Write a SQL query to show all details of the Prime Ministerial winners after 1972 of Menachem Begin and Yitzhak Rabin.   
Sample table: nobel_win

* Answer : 

18. Write a SQL query to show all the details of the winners with first name Louis.    
Sample table: nobel_win

* Answer : 

19. Write a SQL query to show all the winners in Physics for 1970 together with the winner of Economics for 1971.   
Sample table: nobel_win

* Answer : 

20. Write a SQL query to show all the winners of nobel prize in the year 1970 except the subject Physiology and Economics.   
Sample table: nobel_win

* Answer : 

21. Write a SQL query to show the winners of a 'Physiology' prize in an early year before 1971 together with winners of a 'Peace' prize in a later year on and after the 1974.   
Sample table: nobel_win

* Answer : 

22. Write a SQL query to find all details of the prize won by Johannes Georg Bednorz.    
Sample table: nobel_win

* Answer : 

23. Write a SQL query to find all the details of the nobel winners for the subject not started with the letter 'P' and arranged the list as the most recent comes first, then by name in order.    
Sample table: nobel_win

* Answer : 

24. Write a SQL query to find all the details of 1970 winners by the ordered to subject and winner name; but the list contain the subject Economics and Chemistry at last.    
Sample table: nobel_win

* Answer : 

25. Write a SQL query to find all the products with a price between Rs.200 and Rs.600.    
Sample table: item_mast

* Answer : 

26. Write a SQL query to calculate the average price of all products of the manufacturer which code is 16.    
Sample table: item_mast

* Answer : 

27. Write a SQL query to find the item name and price in Rs.    
Sample table: item_mast

* Answer : 

28. Write a SQL query to display the name and price of all the items with a price is equal or more than Rs.250, and the list contain the larger price first and then by name in ascending order.    
Sample table: item_mast

* Answer : 

29. Write a SQL query to display the average price of the items for each company, showing only the company code.  
Sample table: item_mast

* Answer : 

30. Write a SQL query to find the name and price of the cheapest item(s).   
Sample table: item_mast

* Answer : 

31. Write a query in SQL to find the last name of all employees, without duplicates.   
Sample table: emp_details

* Answer : 

32. Write a query in SQL to find the data of employees whose last name is 'Snares'.   
Sample table: emp_details

* Answer : 

33. Write a query in SQL to display all the data of employees that work in the department 57.   
Sample table: emp_details

* Answer : 
