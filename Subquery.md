# Subquery

1. Write a query to find the names (first_name, last_name) and salaries of the employees who have a higher salary than the employee whose last_name='Bull'. Go to the editor
Sample table : employees

* Answer :

2. Write a query to find the names (first_name, last_name) of all employees who works in the IT department. Go to the editor
Sample table : employees

* Answer :

3. Write a query to find the names (first_name, last_name) of the employees who have a manager who works for a department based in the United States. Go to the editor
Hint : Write single-row and multiple-row subqueries
Sample table : employees
Sample table : departments
Sample table : locations

* Answer :

4. Write a query to find the names (first_name, last_name) of the employees who are managers. Go to the editor
Sample table : employees

* Answer :

5. Write a query to find the names (first_name, last_name), the salary of the employees whose salary is greater than the average salary. Go to the editor
Sample table : employees

* Answer :

6. Write a query to find the names (first_name, last_name), the salary of the employees whose salary is equal to the minimum salary for their job grade. Go to the editor
Sample table : employees
Sample table: jobs

* Answer :

7. Write a query to find the names (first_name, last_name), the salary of the employees who earn more than the average salary and who works in any of the IT departments. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

8. Write a query to find the names (first_name, last_name), the salary of the employees who earn more than Mr. Bell. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

9. Write a query to find the names (first_name, last_name), the salary of the employees who earn the same salary as the minimum salary for all departments. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

10. Write a query to find the names (first_name, last_name) of the employees who are not supervisors. Go to the editor
Sample table : employees

* Answer :

11. Write a query to display the employee ID, first name, last names, salary of all employees whose salary is above average for their departments. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

12. Write a query to find the 5th maximum salary in the employees table. Go to the editor
Sample table : employees

* Answer :

13. Write a query to find the 4th minimum salary in the employees table. Go to the editor
Sample table : employees

* Answer :

14. Write a query to select last 10 records from a table. Go to the editor
Sample table : employees

* Answer :

15. Write a query to list department number, name for all the departments in which there are no employees in the department. Go to the editor
Sample table : employees
Sample table : departments

* Answer :

16. Write a query to get 3 maximum salaries. Go to the editor
Sample table : employees

* Answer :

17. Write a query to get 3 minimum salaries. Go to the editor
Sample table : employees

* Answer :

18. Write a query to get nth max salaries of employees. Go to the editor
Sample table : employees

* Answer :
